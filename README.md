# All About Recommendation Engines!

## Objective

<mark>To be filled</mark>  


## Installation/Setup
1. Either clone this repository to a machine of your choice (e.g. your laptop), or to SAS Studio on a [Viya 4 environment using Git integration](https://go.documentation.sas.com/doc/en/webeditorcdc/default/webeditorug/p0puc7muifjjycn1uemlm9lj1jkt.htm).  
   - Here's a useful [video](https://www.youtube.com/watch?v=CZwT7kxmBmw) that takes you through the steps for Git integration.
2. Load the data provided in the repository (within the [data folder](./data/)) to a CAS table / SAS datasets as needed.
3. Import the transfer package found within the [transfer-packages folder](./transfer-packages/) to the SAS Viya environment, using any of the following methods.
    1. Use Environment Manager. Refer instructions [here](https://go.documentation.sas.com/doc/en/sasadmincdc/default/calcontentmig3x/n0djzpossyj6rrn1vvi1wfvp2qhp.htm).
        - Also refer this animated GIF (thanks to Gerry Nelson of SAS) which shows you how to import a transfer package into Environment Manager.
        <kbd>![Import](./img/import_content.gif)</kbd>
    2. Alternatively, use the sas-viya Command Line Interface (a quick video [here](https://www.youtube.com/watch?v=iYi-CoineJY)).
4. You can also use the "Hydrate Content" custom step to carry out steps #2 (for CAS tables) and #3. Here's the most recent location of the [Hydrate Content custom step](https://gitlab.sas.com/SAS-AP/projects/content-hydration/content-hydration-custom-step).

## Additional Tools / Resources
1. [Git Related Custom Steps](https://communities.sas.com/t5/SAS-Communities-Library/Git-Set-N-Go-SAS-Studio-Custom-Steps-for-Git-integration/ta-p/857806)
2. [Loading data from a filesystem folder to CAS](https://communities.sas.com/t5/SAS-Communities-Library/The-quot-CAS-Load-Tables-from-Folders-in-Filesystem-quot-Custom/ta-p/852567)


## Support
- [Sundaresh Sankaran](mailto:sundaresh.sankaran@sas.com)

## Contact
- For questions relating to the use of this repo, get in touch with [Sundaresh Sankaran](mailto:sundaresh.sankaran@sas.com)
- For questions regarding demo content, contact [Suneel Grover](mailto:suneel.grover@sas.com)

### Further acknowledgements:
- [Gerry Nelson](mailto:) - Transfer Packages
- [Danny Zimmerman](mailto:) - Git Integration